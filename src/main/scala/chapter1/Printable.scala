package chapter1

trait Printable[A] {
  def format(a: A): String
}

object PrintableInstances {
  implicit val stringPrintable: Printable[String] = (a: String) => a

  implicit val intPrintable: Printable[Int] = (a: Int) => a.toString
}

object Printable {
  def format[A](a: A)(implicit printable: Printable[A]): String = {
    printable.format(a)
  }

  def print[A](a: A)(implicit printable: Printable[A]): Unit = {
    println(format(a))
  }
}

object PrintableSyntax {
  implicit class PrintableOps[A](a: A) {
    def format(implicit printable: Printable[A]): String = printable.format(a)
    def print(implicit printable: Printable[A]): Unit = println(printable.format(a))
  }
}