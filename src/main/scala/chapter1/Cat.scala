package chapter1

import cats._
import cats.implicits._

final case class Cat(name: String, age: Int, colour: String)

object Cat {
  implicit val printable = new Printable[Cat] {
    override def format(c: Cat) = s"${c.name} is a ${c.age} year-old ${c.colour} cat."
  }

  implicit val show: Show[Cat] = Show.show(c => s"${c.name} is a ${c.age} year-old ${c.colour} cat.")

  implicit val eq: Eq[Cat] = Eq.instance[Cat] { (cat1, cat2) =>
    cat1.name === cat2.name && cat1.age === cat2.age && cat1.colour === cat2.colour
  }
}