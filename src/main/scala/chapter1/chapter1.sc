import cats._
import cats.implicits._

import chapter1.{Cat, Printable}
import chapter1.PrintableInstances._
import chapter1.PrintableSyntax._

Printable.format("123")
Printable.format(123)


val cat = Cat("Andrew", 26, "pale")
Printable.print(cat)
cat.print

cat.show

cat === cat
cat.copy(name = "Harish") === cat
Option(cat) === None
Option(cat.copy(name = "Harish")) == Option(cat)
Option(cat) === Option(cat)


