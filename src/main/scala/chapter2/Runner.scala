package chapter2

object Runner extends App {

  import chapter2.Monoids._

  val list = List(
    Monoid.associativeLaw(true, false, true)(OrMonoid),
    Monoid.associativeLaw(false, false, true)(OrMonoid),
    Monoid.associativeLaw(false, true, false)(OrMonoid),
    Monoid.identityLaw(true)(OrMonoid),
    Monoid.identityLaw(false)(OrMonoid),

    Monoid.associativeLaw(1,2,3)(AdditionMonoid),
    Monoid.identityLaw(1)(AdditionMonoid),

    Monoid.associativeLaw(Set(1,2), Set(2,3), Set(1,3,4))(SetCombineMonoid),
    Monoid.identityLaw(Set(1,2,3,4))(SetCombineMonoid)
  )

  list.foreach(println)

}
