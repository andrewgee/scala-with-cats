package chapter2

object Monoids {
  val OrMonoid = new Monoid[Boolean] {
    override def empty = false

    override def combine(x: Boolean, y: Boolean) = x || y
  }

  val AdditionMonoid = new Monoid[Int] {
    override def empty = 0

    override def combine(x: Int, y: Int) = x + y
  }

  def SetCombineMonoid[A] = new Monoid[Set[A]] {
    override def empty = Set.empty[A]

    override def combine(x: Set[A], y: Set[A]) = x ++ y
  }
}

