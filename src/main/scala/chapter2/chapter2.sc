import cats._
import cats.implicits._

case class Order(totalCost: Double, quantity: Double)

object Order {
  implicit val totalMonoid = new Monoid[Order] {
    override def empty = Order(0,0)

    override def combine(x: Order, y: Order) =
      Order(x.totalCost + y.totalCost, x.quantity + y.quantity)
  }
}

def add[A: Monoid](items: List[A]): A =
  items.foldLeft(Monoid[A].empty)(_ |+| _)

add(List(1,2,3,5))
add(List(Option(1), Option(2), Option(3), Option.empty[Int], Option(5)))

add(List(Order(1,2), Order(3,4)))